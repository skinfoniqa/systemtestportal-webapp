/*
   This file is part of SystemTestPortal.
   Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

   SystemTestPortal is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SystemTestPortal is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package storeuploads

import (
	"encoding/base64"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

func WriteUploadToFile(filename string, fileSource string, project project.Project) (string, error) {
	uploadFolder := path.Join(DirProjects, escape(project.Name), DirUploads)

	files, _ := ioutil.ReadDir(uploadFolder)

	uploadPath, err := writeUploadToFile(fileSource, uploadFolder, strconv.Itoa(len(files)+1)+"_"+filename)
	if err != nil {
		return "", err
	}

	return uploadPath, nil
}

func writeUploadToFile(base64Str string, uploadFolder string, uploadName string) (string, error) {

	uploadFolderAbsolute := path.Join(config.Get().DataDir, uploadFolder)

	err := os.MkdirAll(uploadFolderAbsolute, os.ModePerm)
	if err != nil {
		return "", err
	}

	uploadContent, _ := transformBase64ToFileContentAndExtension(base64Str)

	uploadBytes, err := base64.StdEncoding.DecodeString(uploadContent)
	if err != nil {
		return "", err
	}

	uploadPath := path.Join(uploadFolderAbsolute, uploadName)
	err = ioutil.WriteFile(uploadPath, uploadBytes, os.ModePerm)
	if err != nil {
		return "", err
	}

	return strings.Replace(uploadPath, config.Get().DataDir, "", 1), nil
}
