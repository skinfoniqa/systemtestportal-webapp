/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"github.com/go-xorm/xorm"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity/activityItems"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type TestCreationSQL struct {
	engine *xorm.Engine
}

func (testCreationSQL TestCreationSQL) Create(session *xorm.Session, activityType int, activityEntities *activity.ActivityEntities, request *http.Request) (int64, error) {

	newTestCreation := &activityItems.TestCreation{}

	switch activityType {
	case activity.CREATE_CASE:
		newTestCreation.TestId = activityEntities.Case.Id
		newTestCreation.IsCase = true
	case activity.CREATE_SEQUENCE:
		newTestCreation.TestId = activityEntities.Sequence.Id
		newTestCreation.IsCase = false
	default:
		return 0, errors.Errorf("%s", "Could not find the specified test-creation-activity type")
	}

	_, err := session.Insert(newTestCreation)
	if err != nil {
		return 0, err
	}

	return newTestCreation.Id , nil
}

func (testCreationSQL TestCreationSQL) GetLazyLoadedActivityItem(activity *activity.Activity) (activity.ActivityItem, error) {

	testCreation := activityItems.TestCreation{}

	// First load the TestCreationItem
	has, err := testCreationSQL.engine.Where("id = ?", activity.ActivityItemId).Get(&testCreation)
	if err != nil {
		return nil, err
	} else if !has {
		err = errors.Errorf("%s%v%s", "The Activity with ID: ", activity.ActivityItemId, " has no activity-item")
		return nil, err
	}

	// Load the Test -------------------------------------------------------------------------------|
	if testCreation.IsCase {
		testCase := &test.Case{}
		has, err := testCreationSQL.engine.Table("test_cases").Where("id = ?", testCreation.TestId).Get(testCase)
		if err != nil {
			return nil, err
		} else if !has {
			testCase = nil
		}

		//CAUTION - Since we reuse unique ids, the grabbed testcase could be false. Check the creation date to be sure.
		if testCase != nil {
			if testCase.CreatedAt.After(testCreation.UpdatedAt){
				testCase = nil
			}
		}

		testCreation.TestCase = testCase
	} else {
		testSequence := &test.Sequence{}
		has, err := testCreationSQL.engine.Table("test_sequences").Where("id = ?", testCreation.TestId).Get(testSequence)
		if err != nil {
			return nil, err
		} else if !has {
			testSequence = nil
		}

		//CAUTION - Since we reuse unique ids, the grabbed testcase could be false. Check the creation date to be sure.
		if testSequence != nil {
			if testSequence.CreatedAt.After(testCreation.UpdatedAt){
				testSequence = nil
			}
		}

		testCreation.TestSequence = testSequence
	}
	// ---------------------------------------------------------------------------------------------|

	return testCreation, nil
}
