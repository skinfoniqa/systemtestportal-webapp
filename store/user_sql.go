// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/go-xorm/xorm"
	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store/storeuploads"
	"golang.org/x/crypto/bcrypt"
)

type UsersSQL struct {
	e *xorm.Engine
}

type userRow struct {
	ID                int64
	Name              string
	DisplayName       string
	Email             string
	RegistrationDate  time.Time
	IsAdmin           bool
	Biography         string
	IsShownPublic     bool
	IsEmailPublic     bool
	ProjectHeaderMode int32
	Image             string
	IsDeactivated     bool
}

func userFromRow(ur userRow) *user.User {
	return &user.User{
		Name:              ur.Name,
		DisplayName:       ur.DisplayName,
		Email:             ur.Email,
		RegistrationDate:  ur.RegistrationDate,
		IsAdmin:           ur.IsAdmin,
		Biography:         ur.Biography,
		IsShownPublic:     ur.IsShownPublic,
		IsEmailPublic:     ur.IsEmailPublic,
		ProjectHeaderMode: ur.ProjectHeaderMode,
		Image:             ur.Image,
		IsDeactivated:     ur.IsDeactivated,
	}
}

type userPasswordRow struct {
	ID                int64
	Name              string
	DisplayName       string
	Email             string
	RegistrationDate  time.Time
	IsAdmin           bool
	Biography         string
	IsShownPublic     bool
	IsEmailPublic     bool
	ProjectHeaderMode int32
	Image             string
	IsDeactivated     bool
	PasswordHash      []byte
}

func rowFromPasswordUser(u *user.PasswordUser) userPasswordRow {
	return userPasswordRow{
		Name:              u.Name,
		DisplayName:       u.DisplayName,
		Email:             u.Email,
		RegistrationDate:  u.RegistrationDate,
		IsAdmin:           u.IsAdmin,
		Biography:         u.Biography,
		IsShownPublic:     u.IsShownPublic,
		IsEmailPublic:     u.IsEmailPublic,
		ProjectHeaderMode: u.ProjectHeaderMode,
		Image:             u.Image,
		IsDeactivated:     u.IsDeactivated,
	}
}

func (upr userPasswordRow) asUserRow() userRow {
	return userRow{
		ID:               upr.ID,
		Name:             upr.Name,
		DisplayName:      upr.DisplayName,
		Email:            upr.Email,
		RegistrationDate: upr.RegistrationDate,
		IsAdmin:          upr.IsAdmin,
		Biography:        upr.Biography,
		IsShownPublic:    upr.IsShownPublic,
		IsEmailPublic:    upr.IsEmailPublic,
		Image:            upr.Image,
		IsDeactivated:    upr.IsDeactivated,
	}
}

func (us UsersSQL) List() ([]*user.User, error) {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	users, err := listUsers(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return users, s.Commit()
}

func (us UsersSQL) Get(uID id.ActorID) (*user.User, bool, error) {
	return getUser(us.e, uID)
}

func (us UsersSQL) GetByMail(mail string) (*user.User, bool, error) {
	return getUserByMail(us.e, mail)
}

func (us UsersSQL) Add(u *user.PasswordUser) error {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := saveUser(s, u)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (us UsersSQL) Update(u *user.User) error {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := updateUser(s, u)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (us UsersSQL) Deactivate(u *user.User, isDelete bool) error {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	lastAdmin, err := isLastAdmin(s, u)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}
	if lastAdmin {
		err = fmt.Errorf("User is the last admin, can't deactivate!")
		err = multierror.Append(err, s.Rollback())
		return err
	}

	if u.Image != "" {
		err := storeuploads.DeleteImage(u.Image, storeuploads.ImageType.Profile)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
	}

	if isDelete {
		err = deleteUser(s, u)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
		err := anonymizeProtocols(s, u)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
		err = removeUserFromComment(s, u)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
	} else {
		u.Deactivate()
		err = updateUser(s, u)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
		// delete the tasks
		err = deleteTaskListForUser(s, u)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}

		// get projects that the user is a member of and remove the user from the members
		projects, err := listProjectsForMember(s, u.ID())
		for _, project := range projects {
			project.RemoveMember(u)
			saveProject(s, project)
		}
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
	}

	// get and delete projects of this user (owner)
	projects, err := listProjectsForOwner(s, u.ID())
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}
	for _, project := range projects {
		err := deleteProject(s, project.ID())
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return err
		}
	}

	// delete the user as owner, since he no longer owns any projects
	_, err = s.Table(ownerTable).Delete(&ownerRow{Name: u.Name})
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (us UsersSQL) Validate(identifier, password string) (*user.User, bool, error) {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, false, err
	}

	u, ex, err := validateUser(s, identifier, password)
	if err != nil {
		err = multierror.Append(err, s.Rollback())

		return nil, ex, err
	}

	return u, ex, s.Commit()
}

func saveUser(s xorm.Interface, u *user.PasswordUser) error {
	oupr, ex, err := getUserPasswordRow(s, u.ID())
	if err != nil {
		return err
	}

	nupr := rowFromPasswordUser(u)
	nupr.PasswordHash, err = bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	if !ex {
		err = insertUserRow(s, &nupr)
	} else {
		nupr.ID = oupr.ID
		err = updateUserRow(s, &nupr)
	}

	if err != nil {
		return err
	}

	return nil
}

func updateUserRow(s xorm.Interface, upr *userPasswordRow) error {
	ur := upr.asUserRow()
	err := updateOwnerForUser(s, &ur)
	if err != nil {
		return err
	}
	aff, err := s.Table(userTable).ID(upr.ID).AllCols().Update(upr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func updateUser(s xorm.Interface, u *user.User) error {

	ur, ex, err := getUserRow(s, u.ID())
	ur.IsAdmin = u.IsAdmin
	ur.RegistrationDate = u.RegistrationDate
	ur.IsShownPublic = u.IsShownPublic
	ur.IsEmailPublic = u.IsEmailPublic
	ur.Biography = u.Biography
	ur.Email = u.Email
	ur.ProjectHeaderMode = u.ProjectHeaderMode
	ur.Image = u.Image
	ur.IsDeactivated = u.IsDeactivated

	err = updateOwnerForUser(s, &ur)
	if err != nil {
		return err
	}
	if !ex {
		fmt.Errorf("user to update does not exist")
	}

	upr, ex, err := getUserPasswordRow(s, u.ID())
	if err != nil {
		return err
	}
	if !ex {
		fmt.Errorf("user to update does not exist")
	}
	upr.IsShownPublic = ur.IsShownPublic
	upr.IsEmailPublic = ur.IsEmailPublic
	upr.Biography = ur.Biography
	upr.Email = ur.Email
	upr.ProjectHeaderMode = ur.ProjectHeaderMode
	upr.Image = ur.Image
	upr.IsDeactivated = ur.IsDeactivated

	aff, err := s.Table(userTable).ID(upr.ID).AllCols().Update(upr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertUserRow(s xorm.Interface, upr *userPasswordRow) error {
	ur := upr.asUserRow()
	err := insertOwnerForUser(s, &ur)
	if err != nil {
		return err
	}

	upr.ID = ur.ID
	_, err = s.Table(userTable).Insert(upr)

	return err
}

func listUsers(s xorm.Interface) ([]*user.User, error) {
	urs, err := listUserRows(s)

	var us []*user.User
	for _, ur := range urs {
		u := userFromRow(ur)
		us = append(us, u)
	}

	return us, err
}

func getUser(s xorm.Interface, uID id.ActorID) (*user.User, bool, error) {
	ur, ex, err := getUserRow(s, uID)
	if err != nil || !ex {
		return nil, false, err
	}

	return userFromRow(ur), true, nil
}

func getUserByMail(s xorm.Interface, mail string) (*user.User, bool, error) {
	ur, ex, err := getUserRowByMail(s, mail)
	if err != nil || !ex {
		return nil, false, err
	}
	return userFromRow(ur), true, nil
}

func getUserByID(s xorm.Interface, id int64) (*user.User, bool, error) {
	ur, ex, err := getUserRowByID(s, id)
	if err != nil || !ex {
		return nil, false, err
	}

	return userFromRow(ur), true, nil
}

func validateUser(s xorm.Interface, id, pass string) (*user.User, bool, error) {
	upr, ex, err := getUserPasswordRowByIDAndMail(s, id)
	if err != nil || !ex {
		return nil, false, nil
	}

	if upr.IsDeactivated {
		return nil, true, fmt.Errorf("This user has been deactivated")
	}
	err = bcrypt.CompareHashAndPassword(upr.PasswordHash, []byte(pass))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return nil, false, nil
	}

	if err != nil {
		return nil, false, err
	}

	return userFromRow(upr.asUserRow()), true, nil
}

func lookupUserRowIDs(s xorm.Interface, ids ...id.ActorID) ([]int64, error) {
	var rids []int64
	err := s.Table(userTable).Distinct(idField).In(nameField, ids).Find(&rids)
	if err != nil {
		return nil, err
	}

	return rids, nil
}

func lookupUserRowIDForUser(s xorm.Interface, id id.ActorID) (int64, error) {
	rid := userRow{Name: id.Actor()}
	_, err := s.Table(userTable).Get(&rid)
	if err != nil {
		return -1, err
	}

	return rid.ID, nil
}

func listUserRows(s xorm.Interface) ([]userRow, error) {
	var urs []userRow
	err := s.Table(userTable).Asc(nameField).Find(&urs)
	if err != nil {
		return nil, err
	}

	return urs, nil
}

func getUserRow(s xorm.Interface, uID id.ActorID) (userRow, bool, error) {
	ur := userRow{Name: uID.Actor()}
	ex, err := s.Table(userTable).Get(&ur)
	if err != nil || !ex {
		return userRow{}, false, err
	}

	return ur, true, nil
}

func getUserRowByMail(s xorm.Interface, mail string) (userRow, bool, error) {
	ur := userRow{Email: mail}
	ex, err := s.Table(userTable).Get(&ur)
	if err != nil || !ex {
		return userRow{}, false, err
	}

	return ur, true, nil
}

func getUserRowByID(s xorm.Interface, id int64) (userRow, bool, error) {
	ur := userRow{ID: id}
	ex, err := s.Table(userTable).Get(&ur)
	if err != nil || !ex {
		return userRow{}, false, err
	}

	return ur, true, nil
}

func getUserPasswordRow(s xorm.Interface, id id.ActorID) (userPasswordRow, bool, error) {
	upr := userPasswordRow{Name: id.Actor()}
	ex, err := s.Table(userTable).Get(&upr)
	if err != nil || !ex {
		return userPasswordRow{}, false, err
	}

	return upr, true, nil
}

func getUserPasswordRowByIDAndMail(s xorm.Interface, id string) (userPasswordRow, bool, error) {
	upr := userPasswordRow{Name: id}
	ex, err := s.Table(userTable).Get(&upr)
	if err != nil {
		return userPasswordRow{}, false, err
	}

	if ex {
		return upr, true, nil
	}

	upr = userPasswordRow{Email: id}
	ex, err = s.Table(userTable).Get(&upr)
	if err != nil || !ex {
		return userPasswordRow{}, false, err

	}

	return upr, true, nil
}

// sortUsers sorts a slice of user.User lexiographically
func sortUsers(users []*user.User) {
	sort.Slice(users, func(i, j int) bool {
		cmp := strings.Compare(users[i].Name, users[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}

func resetPassword(s xorm.Interface, u *user.User) error {
	pass, err := bcrypt.GenerateFromPassword([]byte("anon"), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	_, err = s.Table(userTable).Update(&userPasswordRow{PasswordHash: pass}, &userPasswordRow{Name: u.Name})
	return err
}

func isLastAdmin(s xorm.Interface, u *user.User) (bool, error) {
	var admins []user.User

	err := s.Table(userTable).Where("is_admin = ?", 1).Find(&admins)
	return u.IsAdmin && len(admins) == 1, err
}

func deleteUser(s xorm.Interface, u *user.User) error {
	_, err := s.Table(userTable).Delete(&userPasswordRow{Name: u.Name})
	return err
}
