{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "modal-tester-assignment"}}
<div class="modal fade" id="modal-tester-assignment" tabindex="-1" role="dialog" aria-labelledby="modal-tester-assignment-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-tester-assignment-label">Select tester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tester-assignment">
                <div class="col">
                    <div class="float-left openAssignments">
                        <h5>{{T "Open Assignments" .}}</h5>
                        <ul class="list-group">
                        {{ if not .Tasks }}
                        <p class="text-muted">{{T "No open assignments" .}}</p>
                        {{ end }}
                        {{ range .Tasks }}
                            <li class="list-group-item mr-3" id="assignment-{{ .Assignee }}-{{ .Index }}">
                                <div class="float-left">
                                    <div>
                                        <span class="assignmentAssignee">{{ .Assignee }}</span>
                                    </div>
                                    <div>
                                        <span class="assignmentVersion" title="The version of the task">{{ .Version }}</span>
                                    </div>
                                    <div>
                                        <span class="assignmentVariant" title="The variant of the task">{{ .Variant }}</span>
                                    </div>
                                </div>
                                <button class="btn btn-danger float-right deleteAssignment"
                                        id="deleteAssignment-{{ .Assignee }}-{{ .Index }}"
                                        title="Set the task as done for the assignee">
                                    {{T "Done" .}}
                                </button>
                            </li>
                        {{ end }}
                        </ul>
                    </div>

                    <div class="float-right newAssignments">
                        <h5>{{T "New Assignments" .}}</h5>
                        {{ $UserMembers := .Project.UserMembers }}

                         {{ if or .TestCaseVersion.Versions .SequenceInfo.Versions }}
                        {{range $user := $UserMembers }}
                            <div class="form-check checkbox-list-element">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input testerSelector" id="{{ .User }}" name="{{ .User }}">
                                    <label class="custom-control-label" for="{{ .User }}">{{ $user.User }}</label>
                                </div>
                                <div id="assignmentVersionVariant-{{ $user.User }}">
                                    <div>
                                        <label for="assignmentVersions-{{ $user.User }}">Version</label>
                                        <select class="custom-select" id="assignmentVersions-{{ $user.User }}" title="Select the version to assign">
                                        </select>
                                    </div>
                                    <div>
                                        <label for="assignmentVariants-{{ $user.User }}">Variant</label>
                                        <select class="custom-select" id="assignmentVariants-{{ $user.User }}" title="Select the variant to assign">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        {{end}}
                        {{ else }}
                            <div class="m-2">
                                <p class="text-muted">
                                    {{T "This Test is not applicable to any system-under-test-versions." .}}
                                    {{T "Edit the Test and select applicable versions." .}}
                                </p>
                            </div>
                        {{ end }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="buttonAssignTester" type="button" class="btn btn-primary">{{T "Save Assignments" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src='/static/js/modal/tester-assignment.js'></script>
<script src="/static/js/project/sut-versions-show.js" integrity="{{sha256 "/static/js/project/sut-versions-show.js"}}"></script>
<script type="text/javascript">
    var versions = {{ .TestCaseVersion.Versions }};
    if (!versions) {
        versions = {{ .TestSequenceVersion.SequenceInfo.Versions }};
    }
    {{range $user := $UserMembers }}
        // Fill the version dropdown for every tester
        fillVersions(versions, "#assignmentVersions-{{ $user.User }}");
        // Fill the dropdown with variants for the initial version. Then listen to changes.
        updateVariantDropdown(versions, "#assignmentVersions-{{ $user.User }}", "#assignmentVariants-{{ $user.User }}");
        // Listen to changes in the drop down menu of the versions
        setVersionDropdownOnChangeListener(versions, "#assignmentVersions-{{ $user.User }}", "#assignmentVariants-{{ $user.User }}");
    {{ end }}
    // Hide the dropdowns of versions and variants for a user if he is not selected as assignee
    $(".testerSelector").each(function () {
        if (!this.checked) {
            $("#assignmentVersionVariant-" + this.id).hide();
        }
    })
</script>
{{end}}