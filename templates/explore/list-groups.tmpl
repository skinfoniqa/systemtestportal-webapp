{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<div class="row searchbar">
    <div class="col-12">
        <form role="search" class="search-form">
            <label class="sr-only">Search</label>
            <div class="input-group flex-nowrap">
                <input id="groups-search-input" type="search" name="s" class="order-1 search-field form-control float-left rounded filter-search" placeholder="Search...">
                {{ if .SignedIn }}
                    <a class="order-3 btn btn-primary align-middle ml-4 float-right" href="/groups/new">
                        + <span class="d-none d-sm-inline">{{ T "New Group" .}}</span>
                    </a>
                {{ end }}
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="m-4">

        <table id="groups-list" class="table w-100" style="opacity: 0">
            <thead>
                <tr>
                    <th class="pl-0 pt-0" style="border-top: none;">
                        <h5>
                            {{ if .SignedIn }}
                                {{ T "Show public, internal and your private groups" .}}
                            {{ else }}
                                {{ T "Show all public groups" .}}
                            {{ end }}
                        </h5>
                    </th>
                </tr>
            </thead>
            <tbody>
                {{ range .Groups }}

                    <tr class="groupLine" id="{{ .Name }}">
                        <td class="pl-0 pr-0">
                            <div class="d-flex flex-row">
                                <div class="mr-3 d-none d-sm-block">
                                    <img src="/static/img/placeholder.svg" class="rounded" alt="project logo" width="115" height="115">
                                </div>
                                <div class="card-block" style="flex: 1;">
                                    {{/*<span class="d-none d-sm-block float-right badge badge-secondary"><span>created </span><time class="timeago" datetime="{{ provideTimeago .CreationDate }}"></time></span>*/}}
                                    <h4 class="card-title filter-by-search">{{ .Name }}</h4>
                                    <p class="card-text">{{ .Description }}</p>
                                </div>
                            </div>
                        </td>
                    </tr>

                {{ else }}

                    <tr>
                        <td>

                            <!-- Placeholder -->
                            <p class="text-center empty-group-list">
                                <span>{{ T "This list contains all visible groups. Currently, there are no groups." .}}</span><br/><br/>
                            {{ if .SignedIn }}
                                <a class="btn btn-primary align-middle" href="/groups/new">
                                    <span>{{ T "New Group" .}}</span>
                                </a>
                            {{ end }}
                            </p>

                        </td>
                    </tr>

                {{ end }}
            </tbody>
        </table>

    </div>
</div>
<script src="/static/js/explore/explore.js" integrity="{{sha256 "/static/js/explore/explore.js"}}"></script>
<script type='text/javascript'>

    //$(".groupLine").click(function(event){useGroupLink(event)});

    $(() => {
        $("#menuExploreGroups").addClass("disabled active");
        $("#navbarExploreGroups").addClass("disabled active");

        const idTable = "groups-list";
        const idSearch = "groups-search-input";

        const listElementsPerPage = 10;
        const listElements = "groups";

        initializeDataTable(idTable, idSearch, listElementsPerPage, listElements)
    });
</script>
{{end}}


