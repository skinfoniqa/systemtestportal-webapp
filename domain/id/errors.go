/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// alreadyExistsErr returns an ErrorMessage detailing that a given name already exists in the system.
func alreadyExistsErr(id interface{}) error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid name",
		"The given name already exists.", nil).
		WithLog(fmt.Sprintf("The name, sent by the client, already exists. ID: %+v", id)).
		WithStackTrace(2).
		Finish()
}

// nonPositiveVersionNr returns an ErrorMessage detailing that a given VersionNr is null or negative, but shouldn't.
func nonPositiveVersionNr() error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid VersionNr",
		fmt.Sprintf("The given VersionNr is null or negative, but shouldn't."), nil).
		WithLog("The VersionNr, send by the client, is null or negative, but shouldn't..").
		WithStackTrace(2).
		Finish()
}

// briefName returns an ErrorMessage detailing that a given name is considered to brief by the system.
func briefName() error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid name",
		"The name should contain at least four characters.", nil).
		WithLog("The name, sent by the client, was too short.").
		WithStackTrace(2).
		Finish()
}

// forbiddenChar returns an ErrorMessage detailing that a given name contains a forbidden character.
func forbiddenChar(c string) error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid name",
		"The given name contains the forbidden character '"+c+"'. "+
			"Please remove or replace this character", nil).
		WithLog("The name, sent by the client, contained forbidden char ('" + c + "').").
		WithStackTrace(2).
		Finish()
}
