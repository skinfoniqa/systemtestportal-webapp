/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"reflect"
	"testing"
)

func TestNewTestEntityID(t *testing.T) {
	owner := NewActorID("testUser")
	project := NewProjectID(owner, "Project-1")
	cNames := []string{
		"TestCase",
		"Test Case 1",
		"Test-Case-1",
		"",
		"   ",
		"§%&%§",
	}
	for _, cName := range cNames {
		id := NewTestID(project, cName, true)
		if !reflect.DeepEqual(project, id.ProjectID) {
			t.Errorf("Project wasn't saved correctly to the new TestID. Expected %v but got %v.",
				project, id.ProjectID)
		}
		if cName != id.Test() {
			t.Errorf("Casename wasn't saved correctly to new TestID. Expected %q but got %q.", cName, id.Test())
		}
		if !id.IsCase() {
			t.Error("New TestID should be the ID of a testCase but is ID of a testsequence")
		}
	}
	id := NewTestID(project, cNames[0], false)
	if id.IsCase() {
		t.Error("New TestID should be the ID of a testsequence but is ID of a testCase")
	}
}

type TestExistenceCheckerStub struct {
	exists bool
	err    error
}

func (tc TestExistenceCheckerStub) Exists(TestID) (bool, error) {
	return tc.exists, tc.err
}
func TestTestID_Validate(t *testing.T) {
	project := NewProjectID(NewActorID("John"), "Ice-cream")
	testData := []struct {
		name          string
		isCase        bool
		returnExists  bool
		returnError   error
		errorExpected bool
	}{
		{"caseName", true, false, nil, false},
		{"tre", true, false, nil, true},
		{"", false, false, nil, true},
		{"&$(=§$", false, false, nil, false},
		{"thisNameAlreadyExists", true, true, nil, true},
		{"name", false, false, fmt.Errorf("i'm an error"), true},
		{"This character is forbidden: /", true, false, nil, true},
		{"This character is forbidden: \\", true, false, nil, true},
		{"This character is forbidden: ?", false, false, nil, true},
		{"This character is forbidden: #", false, false, nil, true},
	}
	for _, set := range testData {
		id := NewTestID(project, set.name, set.isCase)
		result := id.Validate(TestExistenceCheckerStub{set.returnExists, set.returnError})
		if set.errorExpected && (result == nil) {
			t.Errorf("Validate returned no error, but expected one. \nID: %+v", id)
		}
		if !set.errorExpected && (result != nil) {
			t.Errorf("Validate returned an error, but expected none. \nID: %+v \nError: %v", id, result)
		}
	}
}
