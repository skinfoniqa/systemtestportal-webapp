/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------------------------------------------------------------------------------------------|
// TODO: Insert File Description Here
//---------------------------------------------------------------------------------------------------------------------|

package creation

import (
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

/*
Creates a new Label for the given project
*/
func ProjectLabelPost(labelStore handler.Labels) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if c.User == nil || !c.Project.GetPermissions(c.User).EditProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		// Read in the data from the request
		labelId, err := strconv.ParseInt(r.FormValue(httputil.LabelId), 10, 64)
		if err != nil {
			errors.Handle(err, w, r)
		}
		labelName := r.FormValue(httputil.LabelName)
		labelDesc := r.FormValue(httputil.LabelDesc)
		labelColor := r.FormValue(httputil.LabelColor)
		labelTextColor := r.FormValue(httputil.LabelTextColor)

		var newLabel = project.Label{
			Id:          labelId,
			Name:        labelName,
			Description: labelDesc,
			Color:       labelColor,
			TextColor:   labelTextColor,
		}

		newId, err := labelStore.UpdateLabelForProject(&newLabel, c.Project.Id)
		if err != nil {
			errors.Handle(err, w, r)
		}

		// Did we just insert an new label ?
		if newId != -1 {
			// If so return its id to the user
			w.Write([]byte(strconv.FormatInt(newId, 10)))
		}
	}
}

/*
Creates a new test label for the given test and label
*/
func TestLabelPost(labelStore handler.Labels) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		// Read in the data from the request
		labelId, err := strconv.ParseInt(r.FormValue(httputil.LabelId), 10, 64)
		if err != nil {
			errors.Handle(err, w, r)
		}

		// Create a new testLabel based on case or sequence
		if c.Case == nil {

			// Check for sequence permissions
			if c.User == nil || !c.Project.GetPermissions(c.User).EditSequence {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}

			err := labelStore.InsertTestLabel(labelId, c.Sequence.Id, c.Project.Id, false)
			if err != nil {
				errors.Handle(err, w, r)
			}

		} else {

			// Check for case permissions
			if c.User == nil || !c.Project.GetPermissions(c.User).EditCase {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}

			err := labelStore.InsertTestLabel(labelId, c.Case.Id, c.Project.Id, true)
			if err != nil {
				errors.Handle(err, w, r)
			}

		}
	}
}
