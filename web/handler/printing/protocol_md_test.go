/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
package printing

import (
	"net/http"
	"net/url"
	"reflect"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestProtocolCaseMd(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.TestVersion, "1")
	params.Add(httputil.ProtocolNr, "1")

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.ProjectKey:  handler.DummyProjectPrivate,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseProtocolListerMock{}
				return ProtocolCaseMd(p), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(p, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("CaseProtocolLister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseProtocolListerMock{}
				return ProtocolCaseMd(p), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseProtocolListerMock{}
				return ProtocolCaseMd(p), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(p, 0),
				)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodPost, params),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseProtocolListerMock{
					Protocol: *handler.DummyTestCaseExecutionProtocol,
					Protocols: []test.CaseExecutionProtocol{
						*handler.DummyTestCaseExecutionProtocol,
					},
				}
				return ProtocolCaseMd(p), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(p, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, params),
		),
	)
}

func TestProtocolSequenceMd(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.TestVersion, "1")
	params.Add(httputil.ProtocolNr, "1")

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProject,
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         handler.DummyUserUnauthorized,
			middleware.ProjectKey:      handler.DummyProjectPrivate,
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.SequenceProtocolListerMock{}
				l := &handler.CaseProtocolListerMock{}
				c := &handler.CaseGetterMock{}
				return ProtocolSequenceMd(p, l, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(p, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("CaseProtocolLister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.SequenceProtocolListerMock{}
				l := &handler.CaseProtocolListerMock{}
				c := &handler.CaseGetterMock{}
				return ProtocolSequenceMd(p, l, c), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.SequenceProtocolListerMock{}
				l := &handler.CaseProtocolListerMock{}
				c := &handler.CaseGetterMock{}
				return ProtocolSequenceMd(p, l, c), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(p, 0),
				)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodPost, params),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.SequenceProtocolListerMock{}
				l := &handler.CaseProtocolListerMock{
					Protocol: *handler.DummyTestCaseExecutionProtocol,
				}
				c := &handler.CaseGetterMock{}
				return ProtocolSequenceMd(p, l, c), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(p, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, params),
		),
	)
}

func TestBuildMarkdownForCaseProtocol(t *testing.T) {
	mockCase := dummydata.Cases[0]
	mockProtocol := dummydata.CaseProtocols[0]

	markdown, err := buildMarkdownForCaseProtocol(mockProtocol, mockCase)

	if err != nil {
		t.Errorf("Got error while building markdown:\n" + err.Error())
	}
	if reflect.DeepEqual(markdown, "") {
		t.Errorf("Expected markdown string, however it is empty: " + markdown)
	}

}
