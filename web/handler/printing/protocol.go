/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"html/template"
	"io"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ProtocolsListGet serves the page that display the print screen for a list of protocols
func ProtocolsListGet(tcl handler.TestCaseLister, tsl handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tss, err := tsl.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tcs, err := tcl.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		sel := list.GetSelection(r)

		printTabProtocolList(w, r, c.Project, tcs, tss, sel)
	}
}

// printTabProtocolList executes either the protocol list fragment only or the print
// protocol list fragment with all its parent fragments, depending on the isFrag parameter
func printTabProtocolList(w io.Writer, r *http.Request,
	p *project.Project, tcs []*test.Case, tss []*test.Sequence, sel list.Selection) {

	tmpl := getPrintProtocolListTemplate(r)

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, sel.SelectedCase).
		With(context.SelectedTestSequence, sel.SelectedSequence).
		With(context.SelectedType, sel.SelectedType).
		With(context.Project, p).
		With(context.TestCases, tcs).
		With(context.TestSequences, tss)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

func getPrintProtocolListTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getPrintProtocolsListFragment(r)
	}
	return getPrintProtocolsListTree(r)
}

// getPrintProtocolsListTree returns the
// print protocol list tab template with all parent templates
func getPrintProtocolsListTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence print tree
		Append(templates.PrintProtocols).
		Get().Lookup(templates.HeaderDef)
}

// getPrintProtocolsListFragment returns only the
// print protocol list tab template
func getPrintProtocolsListFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.PrintProtocols).
		Get().Lookup(templates.TabContent)
}

// CaseProtocolsGet shows the template for printing a case protocol
func CaseProtocolsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	sel := list.GetSelection(r)

	printTabTestCaseProtocol(w, r, sel, *c.Project)

}

// printTabTestCaseProtocol executes either the protocol print test case fragment only or the protocol print test case
// fragment with all its parent fragments, depending on the isFrag parameter
func printTabTestCaseProtocol(w io.Writer, r *http.Request, sel list.Selection, p project.Project) {
	tmpl := getPrintCaseProtocolsTemplate(r)

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, sel.SelectedCase).
		With(context.SelectedTestSequence, sel.SelectedSequence).
		With(context.SelectedType, sel.SelectedType).
		With(context.Project, p)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

func getPrintCaseProtocolsTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabProtocolsPrintTestCaseFragment(r)
	}
	return getTabProtocolsPrintTestCaseTree(r)
}

// getTabProtocolsPrintTestCaseFragment returns only the protocol show test case tab template
func getTabProtocolsPrintTestCaseFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.PrintCaseProtocol).
		Get().Lookup(templates.TabContent)
}

// getTabProtocolsPrintTestCaseTree returns the protocol show test case tab template with all parent templates
func getTabProtocolsPrintTestCaseTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.PrintCaseProtocol).
		Get().Lookup(templates.HeaderDef)
}

// SequenceProtocolsGet serves the page for printing testsequence protocols.
func SequenceProtocolsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	sel := list.GetSelection(r)

	printTabTestSequenceProtocol(w, r, sel, *c.Project)

}

// showTabTestSequenceProtocol executes either the protocol show test sequence fragment only or the protocol show
// test sequence fragment with all its parent fragments, depending on the isFrag parameter
func printTabTestSequenceProtocol(w io.Writer, r *http.Request, sel list.Selection, p project.Project) {
	tmpl := getPrintSequenceProtocolTemplate(r)

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, sel.SelectedCase).
		With(context.SelectedTestSequence, sel.SelectedSequence).
		With(context.SelectedType, sel.SelectedType).
		With(context.Project, p)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

func getPrintSequenceProtocolTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getPrintSequenceProtocolsFragment(r)
	}
	return getPrintSequenceProtocolsTree(r)
}

// getPrintSequenceProtocolsFragment returns only the protocol show test sequence tab template
func getPrintSequenceProtocolsFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.PrintSequenceProtocol).
		Get().Lookup(templates.TabContent)
}

// getPrintSequenceProtocolsTree returns the protocol show test sequence tab template with all parent templates
func getPrintSequenceProtocolsTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.PrintSequenceProtocol).
		Get().Lookup(templates.HeaderDef)
}
