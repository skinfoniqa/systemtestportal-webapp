/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package display

import (
	"context"
	"net/http"
	"testing"

	"github.com/dimfeld/httptreemux"

	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestShowProfileGet(t *testing.T) {
	store.InitializeTestDatabase()

	ul := handler.UserListerMock{}
	dummy := handler.DummyUser
	dummy.IsShownPublic = false
	ul.Initialize(dummy, handler.DummyUserUnauthorized)

	ctxUnauthorized := context.Background()
	ctxUnauthorized = httptreemux.AddParamsToContext(ctxUnauthorized, map[string]string{"profile": handler.DummyUserUnauthorized.Name})

	ctxInvalid := context.Background()
	ctxInvalid = httptreemux.AddParamsToContext(ctxInvalid, map[string]string{"profile": "nonexistentuser"})

	ctxPrivate := context.Background()
	ctxPrivate = httptreemux.AddParamsToContext(ctxPrivate, map[string]string{"profile": dummy.Name})

	tested := ShowProfileGet
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleFragmentRequest(ctxUnauthorized, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid Profile",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusNotFound),
			),
			handler.SimpleFragmentRequest(ctxInvalid, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Private Profile",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(ctxPrivate, http.MethodGet, handler.NoParams),
		),
	)
}

func TestEditProfileGet(t *testing.T) {
	store.InitializeTestDatabase()
	ul := handler.UserListerMock{}
	ul.Initialize(handler.DummyUser, handler.DummyUserUnauthorized)
	tested := EditProfileGet

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	unauthorizedCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUserUnauthorized,
		},
	)

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	ctx = httptreemux.AddParamsToContext(ctx, map[string]string{"profile": handler.DummyUser.Name})
	unauthorizedCtx = httptreemux.AddParamsToContext(unauthorizedCtx, map[string]string{"profile": handler.DummyUser.Name})
	invalidCtx = httptreemux.AddParamsToContext(invalidCtx, map[string]string{"profile": "nonexistentuser"})
	noUserCtx := httptreemux.AddParamsToContext(handler.EmptyCtx, map[string]string{"profile": handler.DummyUser.Name})
	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized User",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(unauthorizedCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No User signed in",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleFragmentRequest(noUserCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid Profile",
			handler.ExpectResponse(
				tested(&ul),
				handler.HasStatus(http.StatusNotFound),
			),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
	)
}
