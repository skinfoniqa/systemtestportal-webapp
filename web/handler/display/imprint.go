/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// AboutGet is a handler for showing the about screen
func ImprintGet(w http.ResponseWriter, r *http.Request) {
	tmpl := getImprintTree(r)
	handler.PrintTmpl(context.New().
		WithUserInformation(r), tmpl, w, r)
}

// getAboutTree returns the about screen template with all parent templates
func getImprintTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// About tree
		Append(templates.Imprint).
		Get().Lookup(templates.HeaderDef)
}
