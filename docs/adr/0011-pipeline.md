# Gitlab Pipeline

* Deciders: STP 2.0 Team <!-- optional -->
* Date: documented 09.10.2018 <!-- optional -->

## Decision Outcome

We made muliple decisions during the design of our gitlab CI pipline:

- Cubitruck: the customer required us to do all release builds on a private runner, the cubitruck is a low power pc in the customers office
- Deployment of release files: the customer did not allow the pipeline to push the release files to the ftp directly. 
  So we had to build a ci job that saves the release files to a specific folder that is mounted on the cubitruck to the runner.
  That folder is mirrowed by a script that is locally on the cubitruck to the ftp server.
- Signed Binaries: for security reasons we sign all release binaries and verify their signature before using them to build docker
  containers, etc.
- XGO: The use of sqlite seems to require CGO. Therefore for automatic generation of files for all possible targets we had to use
  xgo. Setting up that ci job was a huge pain. It only runs on x86 and for some reason the xgo job didn't choose the branch on which
  it is executed. It always used master. So we had to fork xgo and add a branch flag.
- Kubernetes: we added demo ci jobs that automatically deploy the STP to a connected gce kubernetes instance. Just add a cluster in
  the gitlab kubernetes tab and your gce credentials. 
