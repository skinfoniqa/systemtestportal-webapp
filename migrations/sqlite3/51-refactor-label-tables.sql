-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
DROP TABLE project_labels;
DROP TABLE test_case_labels;
DROP TABLE test_sequence_labels;

-- +migrate Down
CREATE TABLE project_labels (
    id INTEGER PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    name VARCHAR(255),
    description VARCHAR(255),
    color VARCHAR(6),
    CONSTRAINT uniq_project_id_name UNIQUE (project_id, name)
);

CREATE TABLE test_case_labels (
    id INTEGER PRIMARY KEY,
    test_case INTEGER NOT NULL REFERENCES test_cases(id) ON DELETE CASCADE,
    label INTEGER NOT NULL REFERENCES project_labels(id) ON DELETE CASCADE,
    CONSTRAINT uniq_test_case_label UNIQUE (test_case, label)
);

CREATE TABLE test_sequence_labels (
    id INTEGER PRIMARY KEY,
    test_sequence INTEGER NOT NULL REFERENCES test_sequences(id) ON DELETE CASCADE,
    label INTEGER NOT NULL REFERENCES project_labels(id) ON DELETE CASCADE,
    CONSTRAINT uniq_test_sequence_label UNIQUE (test_sequence, label)
);