/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

var tabMappingTask = Object.freeze({
    OPENED: {url: "opened"},
    CLOSED: {url: "closed"},
});

function initializeTaskTabListener() {

    $("#task-tab-opened").on("click", () => requestTaskTab(tabMappingTask.OPENED));

    $("#task-tab-closed").on("click", () => requestTaskTab(tabMappingTask.CLOSED));

}

/**
 * Requests the task tab.
 * @param taskTab
 */
function requestTaskTab(taskTab) {

    let newUrl = currentURL().removeLastSegments(1).appendSegment(taskTab.url).appendSegment("async");

    $.get(newUrl)
        .done(response => {
            history.pushState('data', '', newUrl.removeLastSegments(1).toString());
            return $("#task-content").empty().append(response);
        })
        .fail(response => {
            $("#modalPlaceholder").empty().append(response.responseText);
            $('#errorModal').modal('show');
        });
}